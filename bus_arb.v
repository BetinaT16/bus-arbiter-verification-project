module bus_arb #(
parameter width=8
)
(
input clk_i,
input reset_i,
input ack_i,
input [width-1:0] dataR_i,
input rq1_i,
input rq2_i,
input rq3_i,
input rq4_i,
input wr1_ni,
input wr2_ni,
input wr3_ni,
input wr4_ni,
input [width-1:0]dataW1_i,
input [width-1:0]dataW2_i,
input [width-1:0]dataW3_i,
input [width-1:0]dataW4_i,
output reg rq_o,
output reg wr_no,
output reg [width-1:0] dataR1_o,
output reg [width-1:0] dataR2_o,
output reg [width-1:0] dataR3_o,
output reg [width-1:0] dataR4_o,
output reg ack1_o,
output reg ack2_o,
output reg ack3_o,
output reg ack4_o,
output reg [width-1:0]dataW_o

);


reg [2:0] client_number;
reg ack_int;

always @(posedge clk_i or posedge reset_i)
if (reset_i)
ack_int<=0;
else 
ack_int<=ack_i;



//Priority arbitration
always @(posedge clk_i or posedge reset_i)
if (reset_i)
client_number<=0;
else if (ack_i | ack_int)
client_number<=0;
else if (client_number == 0) begin
	if (rq1_i==1)
		client_number<=1;
	else if (rq2_i==1)
		client_number<=2;
	else if (rq3_i==1)
		client_number<=3;
	else if (rq4_i==1)
		client_number<=4;
	end



always @(*)
case (client_number)
1 : rq_o<=rq1_i;
2 : rq_o<=rq2_i;
3 : rq_o<=rq3_i;
4 : rq_o<=rq4_i;
default : rq_o<=0;
endcase
 

always @(*)
case (client_number)
1 : wr_no<=wr1_ni;
2 : wr_no<=wr2_ni;
3 : wr_no<=wr3_ni;
4 : wr_no<=wr4_ni;
default : wr_no<=0;
endcase

always @(*)
case(client_number)
1 : dataW_o<=dataW1_i;
2 : dataW_o<=dataW2_i;
3 : dataW_o<=dataW3_i;
4 : dataW_o<=dataW4_i;
default : dataW_o<=0;
endcase

always @(posedge clk_i or posedge reset_i)
if (reset_i)
begin
dataR1_o<=0;
dataR2_o<=0;
dataR3_o<=0;
dataR4_o<=0;
end
else if (client_number=='d1 & wr1_ni )
dataR1_o<=dataR_i;
else if (client_number=='d2 & wr2_ni )
dataR2_o<=dataR_i;
else if (client_number=='d3 & wr3_ni )
dataR3_o<=dataR_i;
else if (client_number=='d4 & wr4_ni )
dataR4_o<=dataR_i;

always @(posedge clk_i or posedge reset_i)
if (reset_i)
begin
ack1_o<=0;
ack2_o<=0;
ack3_o<=0;
ack4_o<=0;
end
else begin
case(client_number)
1 : begin
ack1_o<=ack_i;
ack2_o<=0;
ack3_o<=0;
ack4_o<=0;
end 
2 : begin
ack1_o<=0;
ack2_o<=ack_i;
ack3_o<=0;
ack4_o<=0;
end
3 : begin 
ack1_o<=0;
ack2_o<=0;
ack3_o<=ack_i;
ack4_o<=0;
end
4 :begin
ack1_o<=0;
ack2_o<=0;
ack3_o<=0;
ack4_o<=ack_i;
end
0 : begin
ack1_o<=0;
ack2_o<=0;
ack3_o<=0;
ack4_o<=0;
end

endcase
end

endmodule