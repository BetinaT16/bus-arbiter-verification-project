class req_ack_drv;

virtual req_ack_if vif;

function new(
 virtual req_ack_if vif_new
);
  vif = vif_new;
endfunction : new

task start(int nr_trans=10 );
  vif.req<=0;
  vif.wr_n<=0;
  vif.wdata<=0;
  @(negedge vif.reset_i);
  $display("A pornit driverul");
  for (int i = 0 ; i < nr_trans; i++) begin
    repeat($urandom_range(0,10)) 
	    @(posedge vif.clk_i);

    drive_sig();
  end
endtask : start

task drive_sig;
   vif.req<=1;
   vif.wr_n<=$urandom%2;          
	 vif.wdata<=$urandom;
   @(posedge vif.ack);
   @(posedge vif.clk_i);
   vif.req<=0;
   
endtask : drive_sig

endclass : req_ack_drv