`include "../ve_src/receiver.sv"
`include "../ve_src/driver.sv"
`include "../ve_src/monitor.sv"
`include "../ve_src/sb.sv"
//`include "../ve_src/if.sv"
class req_ack_env;


req_ack_drv cl0_drv;
req_ack_drv cl1_drv;
req_ack_drv cl2_drv;
req_ack_drv cl3_drv;

req_ack_monitor cl0_mon;
req_ack_monitor cl1_mon;
req_ack_monitor cl2_mon;
req_ack_monitor cl3_mon;

req_ack_receiver srv_rcv;
req_ack_monitor  srv_mon;

req_ack_sb       sb;

mailbox#(bit[7:0]) cl0_mb;
mailbox#(bit[7:0]) cl1_mb;
mailbox#(bit[7:0]) cl2_mb;
mailbox#(bit[7:0]) cl3_mb;
mailbox#(bit[7:0]) srv_mb;
mailbox#(bit) cl0_ack_mb;
mailbox#(bit) cl1_ack_mb;
mailbox#(bit) cl2_ack_mb;
mailbox#(bit) cl3_ack_mb;
mailbox#(bit) srv;


function new(
    virtual req_ack_if i_if_cl0,
    virtual req_ack_if i_if_cl1,
    virtual req_ack_if i_if_cl2,
    virtual req_ack_if i_if_cl3,
    virtual req_ack_if i_if_srv
  );
  cl0_mb = new();
  cl1_mb = new();
  cl2_mb = new();
  cl3_mb = new();
  srv_mb = new();
  cl0_ack_mb=new();
  cl1_ack_mb=new();
  cl2_ack_mb=new();
  cl3_ack_mb=new();
  srv=new();

  cl0_drv = new(.vif_new(i_if_cl0));
  cl1_drv = new(.vif_new(i_if_cl1));
  cl2_drv = new(.vif_new(i_if_cl2));
  cl3_drv = new(.vif_new(i_if_cl3));
  cl0_mon = new(.vif_new(i_if_cl0),
                .send_ack_mb_new(cl0_ack_mb),
                .send_data_mb_new(cl0_mb));
  cl1_mon = new(.vif_new(i_if_cl1),
                .send_ack_mb_new(cl1_ack_mb),
                .send_data_mb_new(cl1_mb));
  cl2_mon = new(.vif_new(i_if_cl2),
                .send_ack_mb_new(cl2_ack_mb),
                .send_data_mb_new(cl2_mb));
  cl3_mon = new(.vif_new(i_if_cl3),
                .send_ack_mb_new(cl3_ack_mb),
                .send_data_mb_new(cl3_mb)); 
  srv_mon = new(.vif_new(i_if_srv),
                .send_data_mb_new(srv_mb),
                .send_ack_mb_new(srv));                
  srv_rcv    = new(.vif_new(i_if_srv)); 
  sb = new(.vif_new(i_if_srv),
            .cl0_mb_new(cl0_mb), 
           .cl1_mb_new(cl1_mb),
		       .cl2_mb_new(cl2_mb),
		       .cl3_mb_new(cl3_mb),
		       .srv_mb_new(srv_mb),
           .cl0_ack_mb_new(cl0_ack_mb),
           .cl1_ack_mb_new(cl1_ack_mb),
           .cl2_ack_mb_new(cl2_ack_mb),
           .cl3_ack_mb_new(cl3_ack_mb)
                 
           );
endfunction : new

task start();
  fork

  srv_rcv.start();
	srv_mon.start();
	cl0_mon.start();
	cl1_mon.start();
	cl2_mon.start();
	cl3_mon.start();
	sb.start();

  cl0_drv.start(100);
	cl1_drv.start(100);
	cl2_drv.start(100);
	cl3_drv.start(100);
  join
endtask : start


endclass : req_ack_env
