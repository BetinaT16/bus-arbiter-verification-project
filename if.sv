interface req_ack_if(
    input clk_i,
    input reset_i
);

logic       req;
logic       ack;
logic       wr_n;
logic[7:0] rdata; 
logic[7:0] wdata; 

//Classifier req is not asserted if win ctr req is 0
property p_ack_period_check;
@(posedge clk_i) disable iff(reset_i)
(ack|=>!ack);
endproperty
// acknowledge is not asserted without request
property p_ack_check; 
@(posedge clk_i) disable iff(reset_i)
(!req|=> !ack);
endproperty


//Checks if dataW stable with the request signal
property p_req_wr_data_stable;
@(posedge clk_i) disable iff(reset_i)
((req == 1 & ack == 0 & req ==$past(req))|=>$stable(wdata));//verif b2b
endproperty

//Check all datas reset value
property p_values_reset;
@(posedge clk_i) disable iff(!reset_i)
(!req & !wr_n & !wdata & !rdata & !ack);
endproperty

//verify if a client holds request on 1 logic until he receives acknowledge signal;
property p_req_hold_check;
@(posedge clk_i) disable iff(reset_i)
(req & !ack|=>req);
endproperty

//check if acknowledge is received;
property p_ack_received;
@(posedge clk_i) disable iff(reset_i)
($rose(req)|=>##[0:50]$rose(ack));
endproperty

//checks if data read is valid while acknowledge is active;
property p_ack_rd_data_valid;
@(posedge clk_i) disable iff(reset_i)
(ack|->rdata!=0);
endproperty

//check if between ack signals there is at least one clock cycle;
property p_ack_dist_check;
@(posedge clk_i) disable iff(reset_i)
(ack|=> !ack);
endproperty

property p_b2b_ack_dist_check;
@(posedge clk_i) disable iff(reset_i)
(req&ack##1 req|->!ack);
endproperty




a_ack_period_check    :assert property (p_ack_period_check) else 
                       $display("@%0dns  Assertion a_ack_period_check failed", $time); 
a_ack_check           :assert property (p_ack_check) else 
                       $display("@%0dns  Assertion a_ack_check failed", $time); 
a_req_wr_data_stable  :assert property(p_req_wr_data_stable) else 
                      $display("@%0dns  Assertion a_req_wr_data_stable failed", $time); 
a_values_reset        :assert property(p_values_reset) else 
                      $display("@%0dns  Assertion a_values_reset failed", $time); 
a_req_hold_check      :assert property(p_req_hold_check) else  
                      $display("@%0dns  Assertion a_req_hold_check failed", $time); 
a_ack_received        :assert property(p_ack_received) else 
                      $display("@%0dns  Assertion a_ack_received failed", $time);
a_ack_rd_data_valid   :assert property(p_ack_rd_data_valid) else 
                      $display("@%0dns  Assertion a_ack_rd_data_valid failed", $time);
a_ack_dist_check      :assert property(p_ack_dist_check) else
                      $display("@%0dns  Assertion a_ack_dist_check failed", $time);
a_b2b_ack_dist_check      :assert property(p_b2b_ack_dist_check) else
                      $display("@%0dns  Assertion a_b2b_ack_dist_check failed", $time);

// http://www.asic-world.com/systemverilog/assertions.html
// http://www.asic-world.com/systemverilog/coverage.html

endinterface : req_ack_if