class req_ack_monitor;

virtual req_ack_if vif;

mailbox#(bit[7:0]) send_data_mb;
mailbox#(bit)send_ack_mb;

function new(
  virtual req_ack_if vif_new,
  mailbox#(bit) send_ack_mb_new,
  mailbox#(bit[7:0]) send_data_mb_new
);
  vif = vif_new;
  send_data_mb = send_data_mb_new;
  send_ack_mb=send_ack_mb_new;
endfunction : new

task start();
  @(negedge vif.reset_i);
  forever begin
    monitor_sig();
  end
endtask : start

task monitor_sig;
 bit[7:0] data;
 bit ack_data=0;
 
 @(posedge vif.ack);
 if(!vif.wr_n) 
   data = vif.wdata;
 else 
   data = vif.rdata;
   ack_data=1;
   $display("sending Data %d",data);

 send_ack_mb.put(ack_data);
  send_data_mb.put(data);

endtask : monitor_sig


endclass : req_ack_monitor