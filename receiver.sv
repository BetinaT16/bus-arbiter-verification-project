class req_ack_receiver;

virtual req_ack_if vif;

function new(
virtual req_ack_if vif_new
);
   vif = vif_new;
endfunction : new

task start();
vif.ack<=0;
vif.rdata<=0;
  @(negedge vif.reset_i);
  forever begin
    drive_sig();
  end
endtask : start

task drive_sig();
   wait(vif.req);
   repeat($urandom_range(1,10))  @(posedge vif.clk_i);
   vif.ack<=1;
   if (!vif.wr_n)
      vif.rdata<=vif.wdata;
   else
      vif.rdata<=$urandom;
   @(posedge vif.clk_i);
   vif.ack<=0;
   vif.rdata<=0;

endtask : drive_sig

endclass : req_ack_receiver