class req_ack_sb;
virtual req_ack_if vif;
  mailbox#(bit[7:0]) cl0_mb;
  mailbox#(bit[7:0]) cl1_mb;
  mailbox#(bit[7:0]) cl2_mb;
  mailbox#(bit[7:0]) cl3_mb;
  mailbox#(bit[7:0]) srv_mb;
  mailbox#(bit) cl0_ack_mb;
  mailbox#(bit) cl1_ack_mb;
  mailbox#(bit) cl2_ack_mb;
  mailbox#(bit) cl3_ack_mb;

  function new(
    mailbox#(bit[7:0]) cl0_mb_new,
    mailbox#(bit[7:0]) cl1_mb_new,
    mailbox#(bit[7:0]) cl2_mb_new,
    mailbox#(bit[7:0]) cl3_mb_new,
    mailbox#(bit[7:0]) srv_mb_new,
    mailbox#(bit) cl0_ack_mb_new,
    mailbox#(bit) cl1_ack_mb_new,
    mailbox#(bit) cl2_ack_mb_new,
    mailbox#(bit) cl3_ack_mb_new,
    virtual req_ack_if vif_new
  );
    cl0_mb = cl0_mb_new;
    cl1_mb = cl1_mb_new;
    cl2_mb = cl2_mb_new;
    cl3_mb = cl3_mb_new;
    srv_mb = srv_mb_new;

    cl0_ack_mb = cl0_ack_mb_new;
    cl1_ack_mb = cl1_ack_mb_new;
    cl2_ack_mb = cl2_ack_mb_new;
    cl3_ack_mb = cl3_ack_mb_new;
    vif = vif_new;
    

  endfunction : new


  task start();
  bit[7:0] cl0_data;
	bit[7:0] cl1_data;
	bit[7:0] cl2_data;
	bit[7:0] cl3_data;
  bit[7:0] srv_data;
  bit[2:0] priorit = 4;

  bit cl0_ack_data;
  bit cl1_ack_data;
  bit cl2_ack_data;
  bit cl3_ack_data;
  forever begin
  cl0_ack_data=0;
  cl1_ack_data=0;
  cl2_ack_data=0;
  cl3_ack_data=0;

	@(posedge vif.clk_i);
      cl0_ack_mb.try_get(cl0_ack_data);
      cl1_ack_mb.try_get(cl1_ack_data);
      cl2_ack_mb.try_get(cl2_ack_data);
      cl3_ack_mb.try_get(cl3_ack_data); 
	  
	if(cl0_ack_data==1) priorit=0;
    else if (cl1_ack_data==1) priorit=1;
      else if(cl2_ack_data==1) priorit=2;
	      else if (cl3_ack_data==1) priorit=3;
          else priorit = 4;
	
  if (priorit==0) 
  begin
    cl0_mb.get(cl0_data);
    srv_mb.get(srv_data);
    $display("Data on client %0d at %0t Expected %0h| Received %0h",priorit, $time, srv_data, cl0_data);
	if(cl0_data !=srv_data)
    begin
	  $display("Data mismatch on client %0d at %0t Expected %0h| Received %0h",priorit, $time, srv_data, cl0_data);
    $stop;
    end 
  end
  else  if (priorit==1) 
            begin
            cl1_mb.get(cl1_data);
            srv_mb.get(srv_data);
            $display("Data on client %0d at %0t Expected %0h| Received %0h",priorit, $time, srv_data, cl1_data);
            if(cl1_data !=srv_data)
              begin
	            $display("Data mismatch on client %0d at %0t Expected %0h| Received %0h",priorit, $time, srv_data, cl1_data);
              $stop;
              end
            end
         else  if (priorit==2) 
                      begin
                      cl2_mb.get(cl2_data);
                      srv_mb.get(srv_data);
                      $display("Data on client %0d at %0t Expected %0h| Received %0h",priorit, $time, srv_data, cl2_data);
                        if(cl2_data !=srv_data)
	                      begin
                        $display("Data mismatch on client %0d at %0t Expected %0h| Received %0h",priorit, $time, srv_data, cl2_data);
                        $stop;
                        end 
                      end
                else if (priorit==3) 
                                   begin
                                   cl3_mb.get(cl3_data);
                                   srv_mb.get(srv_data);
                                   $display("Data on client %0d at %0t Expected %0h| Received %0h",priorit, $time, srv_data, cl3_data);
                                     if(cl3_data !=srv_data)
	                                   begin
                                     $display("Data mismatch on client %0d at %0t Expected %0h| Received %0h",priorit, $time, srv_data, cl3_data);
                                     $stop;
                                     end
                                   end
                                    
  end
  endtask : start




endclass : req_ack_sbclass req_ack_sb;

  mailbox#(bit[7:0]) cl0_mb;
  mailbox#(bit[7:0]) cl1_mb;
  mailbox#(bit[7:0]) cl2_mb;
  mailbox#(bit[7:0]) cl3_mb;
  mailbox#(bit[7:0]) srv_mb;
  mailbox#(bit) cl0_ack_mb;
  mailbox#(bit) cl1_ack_mb;
  mailbox#(bit) cl2_ack_mb;
  mailbox#(bit) cl3_ack_mb;

  function new(
    mailbox#(bit[7:0]) cl0_mb_new,
    mailbox#(bit[7:0]) cl1_mb_new,
    mailbox#(bit[7:0]) cl2_mb_new,
    mailbox#(bit[7:0]) cl3_mb_new,
    mailbox#(bit[7:0]) srv_mb_new,
    mailbox#(bit) cl0_ack_mb_new,
    mailbox#(bit) cl1_ack_mb_new,
    mailbox#(bit) cl2_ack_mb_new,
    mailbox#(bit) cl3_ack_mb_new
  );
    cl0_mb = cl0_mb_new;
    cl1_mb = cl1_mb_new;
    cl2_mb = cl2_mb_new;
    cl3_mb = cl3_mb_new;
    srv_mb = srv_mb_new;

    cl0_ack_mb = cl0_ack_mb_new;
    cl1_ack_mb = cl1_ack_mb_new;
    cl2_ack_mb = cl2_ack_mb_new;
    cl3_ack_mb = cl3_ack_mb_new;
    
    

  endfunction : new


  task start();
  int cl0_data=0;
	int cl1_data=0;
	int cl2_data=0;
	int cl3_data=0;
	int srv_data=0;
	int priorit=0;
    forever begin
	fork
      cl0_mb.get(cl0_data);
      cl1_mb.get(cl1_data);
      cl2_mb.get(cl2_data);
      cl3_mb.get(cl3_data); 
      srv_mb.get(srv_data);
	join_any
	  
	if(priorit==0)
	cl0_mb.get(cl0_data);
	else if(priorit==1)
	cl1_mb.get(cl1_data);
	else if(priorit==2)
	cl2_mb.get(cl2_data);
	else if(priorit==3)
	cl3_mb.get(cl3_data);
	else 
	srv_mb.get(srv_data);
	
	if(cl0_data !=srv_data)
	$display("Data mismatch Expected%0h| Received %0h", srv_data, cl0_data);
	else if(cl1_data !=srv_data)
	$display("Data mismatch Expected%0h| Received %0h", srv_data, cl1_data);
	else if(cl2_data !=srv_data)
	$display("Data mismatch Expected%0h| Received %0h", srv_data, cl2_data);
	else if(cl3_data !=srv_data)
	$display("Data mismatch Expected%0h| Received %0h", srv_data, cl3_data);
	 
    end 
  endtask : start

endclass : req_ack_sb